package service

import (
	"example/goproject/database"
	"example/goproject/model"
)

type BookService struct{}

func NewBookService() *BookService {
	return &BookService{}
}

func (service *BookService) GetAllBooks() ([]model.Book, error) {
	var books []model.Book
	err := database.Instance.Find(&books).Error
	if err != nil {
		return books, err
	}
	return books, nil
}

func (service *BookService) CreateBook(book model.Book) error {
	err := database.Instance.Create(&book).Error
	if err != nil {
		return err
	}
	return nil
}

func (service *BookService) GetBook(id string) (model.Book, error) {
	var book model.Book
	err := database.Instance.First(&book, id).Error
	if err != nil {
		return book, err
	}
	return book, nil
}

func (service *BookService) DeleteBook(id string) error {
	err := database.Instance.Delete(model.Book{}, id).Error
	if err != nil {
		return err
	}
	return nil

}

func (service *BookService) UpdateBook(book model.Book) (model.Book, error) {
	var updateBook model.Book
	err := database.Instance.First(&updateBook, book.Id).Error
	if err != nil {
		return updateBook, err
	}
	updateError := database.Instance.Model(&updateBook).Updates(book).Error
	if updateError != nil {
		return updateBook, updateError
	}
	return updateBook, nil
}
