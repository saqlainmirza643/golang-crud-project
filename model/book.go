package model

type Book struct {
	Id       uint   `gorm:"primary_key" json:"id"`
	Title    string `json:"title"`
	Author   string `json:"author"`
	Quantity int    `json:"quantity"`
}
