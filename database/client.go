package database

import (
	"example/goproject/model"
	"log"
	"os"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var Instance *gorm.DB
var err error

func LoadDatabase() {
	dburl := os.Getenv("DB_URL")
	dsn, err := gorm.Open(mysql.Open(dburl), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
		panic("Cannot connect to DB")
	}
	log.Println("Connected to Database...")
	Instance = dsn
}

func Migrate() {
	Instance.AutoMigrate(&model.Book{})
	log.Println("Database Migration Completed...")
}
