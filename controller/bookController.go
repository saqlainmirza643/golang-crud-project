package controller

import (
	"example/goproject/model"
	"example/goproject/service"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type BookController struct {
	service service.BookService
}

func NewBookController(service service.BookService) *BookController {
	return &BookController{service: service}
}

func (controller *BookController) CreateBook(c *gin.Context) {
	var body struct {
		Title    string
		Author   string
		Quantity int
	}
	c.Bind(&body)

	book := model.Book{Title: body.Title, Author: body.Author, Quantity: body.Quantity}

	err := controller.service.CreateBook(book)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Status": "Inserted Succesfully",
	})

	c.JSON(200, gin.H{
		"Book": book,
	})

}
func (controller *BookController) GetAllBooks(c *gin.Context) {
	books, err := controller.service.GetAllBooks()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Books": books,
	})
}

func (controller *BookController) GetBook(c *gin.Context) {

	id := c.Query("id")

	book, err := controller.service.GetBook(id)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Book": book,
	})
}

func (controller *BookController) DeleteBook(c *gin.Context) {
	id := c.Param("id")
	err := controller.service.DeleteBook(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Status": "Deleted Succesfully",
	})
}

func (controller *BookController) UpdateBook(c *gin.Context) {
	var body struct {
		Id       uint
		Title    string
		Author   string
		Quantity int
	}
	c.Bind(&body)
	upadatedData := model.Book{Id: body.Id, Title: body.Title, Author: body.Author, Quantity: body.Quantity}
	fmt.Print(upadatedData)
	book, err := controller.service.UpdateBook(upadatedData)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Books": book,
	})
}
