package routes

import (
	"example/goproject/controller"
	"example/goproject/service"

	"github.com/gin-gonic/gin"
)

func SetupRoutes(r *gin.Engine) {
	bookController := controller.NewBookController(*service.NewBookService())
	r.GET("/books", bookController.GetAllBooks)
	r.POST("/createBook", bookController.CreateBook)
	r.GET("/getBook", bookController.GetBook)
	r.DELETE("/deleteBook/:id", bookController.DeleteBook)
	r.PUT("/updateBook", bookController.UpdateBook)
}
